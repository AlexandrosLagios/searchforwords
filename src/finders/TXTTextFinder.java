package finders;

import input.FileToSearch;
import view.TextSearchResults;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TXTTextFinder extends TextFinder
{

    public TXTTextFinder(FileToSearch file)
    {
        super(file);
    }

    @Override
    public List<TextSearchResults> findTextInDocument() throws IOException
    {
        List<TextSearchResults> results = new ArrayList<>();

        for (String text: textList)
        {
            results.add(findTextInTXT(text));
        }

        return results;
    }

    private TextSearchResults findTextInTXT(String text) throws IOException
    {
        List<Integer> locationList = new ArrayList<>();

        BufferedReader reader;
        reader = new BufferedReader(new FileReader(file.getFile()));
        String line = reader.readLine();
        int lineNumber = 0;

        while (line != null)
        {
            lineNumber++;
            if(line.toLowerCase().contains(text))
            {
                locationList.add(lineNumber);
            }
            line = reader.readLine();
        }

        reader.close();

        return getTextSearchResults(text, locationList);
    }
}
