package finders;

import input.FileToSearch;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.xmlbeans.XmlException;
import view.TextSearchResults;

import java.io.IOException;
import java.util.List;

/**
 * The parent class for all the classes that search in the different types of document.
 */
public abstract class TextFinder
{
    protected FileToSearch file;
    protected List<String> textList;


    public TextFinder() {}

    /**
     * An object that can search for a list of pieces of text in a document.
     * @param file The file we are going to search in.
     */
    public TextFinder(FileToSearch file)
    {
        this.file = file;
    }


    public List<String> getTextList()
    {
        return textList;
    }

    public void setTextList(List<String> textList)
    {
        this.textList = textList;
    }

    public FileToSearch getFile()
    {
        return file;
    }

    /**
     * Finds where every element of the text list is located in a document.
     * @return a list of every location that every piece of text was found in.
     * @throws IOException
     */
    public abstract List<TextSearchResults> findTextInDocument() throws IOException, OpenXML4JException, XmlException;

    /**
     * Returns a TextSearchResults object for a given text and a collection of locations that the text was found in.
     * @param text The text we were searching for.
     * @param locationList The list of locations the text was found.
     * @return A TextSearchResults object. For that object, we have determined if the wasFound attribute is true
     * (it is false if the locationList is null) and we have assigned the locationList to its locationList attribute.
     */
    public TextSearchResults getTextSearchResults(String text, List<Integer> locationList)
    {
        TextSearchResults textSearchResults = new TextSearchResults(text);

        if (locationList.size() == 0)
        {
            textSearchResults.setWasFound(false);
        }
        else
        {
            textSearchResults.setWasFound(true);
        }

        textSearchResults.setLocations(locationList);

        return textSearchResults;
    }
}
