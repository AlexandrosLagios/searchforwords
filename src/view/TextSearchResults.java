package view;

import org.apache.poi.ss.formula.functions.T;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * A class that contains the search results for a given text, namely if the text was found
 * and the locations where it was found.
 * @param <T> A generic type that will collect the locations. It can be an ArrayList of Integers or
 *           a HashMap that matches Strings (sheet names) with an ArrayList of Integers
 *           (the column numbers that the text was found in).
 */
public class TextSearchResults <T extends Object>
{
    private String text;
    private T locations;
    private boolean wasFound;

    public TextSearchResults(String text)
    {
        this.text = text;
    }

    public void setLocations(T locations)
    {
        this.locations = locations;
    }

    public String getText()
    {
        return text;
    }

    public T getLocations()
    {
        return locations;
    }

    public boolean wasfound()
    {
        return wasFound;
    }

    public void setWasFound(boolean wasFound)
    {
        this.wasFound = wasFound;
    }
}