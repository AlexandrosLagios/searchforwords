package input;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class TextToSearch
{
    public List<String> getTextList()
    {
        int moreText = 0;
        List<String> textList = new ArrayList<>();

        while (moreText != 1)
        {
            JLabel label = new JLabel("Please write a of text we will search for:");
            label.setFont(new Font("Dialog", Font.BOLD,16));
            String text = JOptionPane.showInputDialog(label);
            textList.add(text.toLowerCase());
            label = new JLabel("Do you want search for another text?");
            label.setFont(new Font("Dialog", Font.BOLD,16));
            moreText = JOptionPane.showConfirmDialog(null, label,
                    "More Text?", JOptionPane.YES_NO_OPTION);
        }
        return textList;
    }
}
