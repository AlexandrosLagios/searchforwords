package input;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * A class that opens an explorer in order for us to choose a file or directory.
 */
public class FileOrDirectoryChooser extends JFrame
{
    List<File> allTheFiles = new ArrayList<>();

    public List<File> getAllTheFiles()
    {
        getFilesFromPath(getPath());
        return this.allTheFiles;
    }

    public File getPath()
    {
        JFileChooser fileOrDirectoryChooser = new JFileChooser();

        fileOrDirectoryChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        fileOrDirectoryChooser.setDialogTitle("Choose a file or directory to search");
        int result = fileOrDirectoryChooser.showOpenDialog(this);

        if(result == JFileChooser.CANCEL_OPTION)
        {
            System.exit(1);
        }

        File file = fileOrDirectoryChooser.getSelectedFile();

        return file;
    }

    public void getFilesFromPath(File file)
    {
        if(file.isDirectory())
        {
            for(File fileInDirectory: file.listFiles())
            {
                getFilesFromPath(fileInDirectory);
            }
        }
        else
        {
            allTheFiles.add(file);
        }
    }
}
